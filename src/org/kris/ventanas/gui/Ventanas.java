package org.kris.ventanas.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ventanas {
    private JPanel panel1;
    private JButton bntRegistrar;
    private JTextField tfnombre;
    private JTextField tfApellidos;
    private JTextField tfFechaNacimiento;
    private JTextField tfAltura;
    private JList list1;
    private JRadioButton rbMasculino;
    private JRadioButton rbFemenino;
    private JButton btnLimpiar;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ventanas");
        frame.setContentPane(new Ventanas().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public Ventanas() {


        bntRegistrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre = tfnombre.getText();
                String apellido = tfApellidos.getText();
                String fechaNacimiento = tfFechaNacimiento.getText();
                String altura = tfAltura.getText();
                String masculino = rbMasculino.getText();
                String femenino = rbFemenino.getText();

                System.out.print(nombre+" "+apellido+" "+fechaNacimiento+" "+altura);
            }
        });
    }
}
