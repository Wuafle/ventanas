package Proyects;

import prueba1.Bicicleta;
import prueba1.Carro;

public class Main {
    public static void main(String[] args){

        Carro c = new Carro();
        Bicicleta b = new Bicicleta();

        c.avanzar();
        b.avanzar();
        b.sentarse();
    }
}
